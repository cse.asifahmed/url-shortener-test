# url-shortener-test
Url shortener assessment for the position of Software Engineer (Java), given by OrangeToolz Tech.

## Used technology
```bash
* Java 8
* Spring Boot 2.3.9
* H2 database(In memory database)
```

## Communication protocol
```bash
* RESTful API.
```

## How to run?
```bash
* Download zip file and extract it.
* Import the unzip folder as maven project to a `IDE` like `Eclipse` or `Intellij Idea`.
* First run the project as "Java Application"
* Use below mentioned URLs from postman or a browser.
```

## Necessary api link:
```bash
* POST: http://localhost:8080/api/path?arg1=you&arg2=tube (Inside body add long url like https://www.youtube.com/)
* GET : http://localhost:8080/api/path/get-all
* GET (by shortUrl): http://localhost:8080/api/path/get-by-short-url?shortUrl=bVVtsk
* GET (count all urls, stored in db): http://localhost:8080/api/path/count
```

## Future task
```bash
* Code optimize
* Unit test integration
* Proper documentation
* Prepare logframe
```

